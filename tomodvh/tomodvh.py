from rxcheck_base_importer import BaseImporter

import dvh
import numpy
import pandas
import os
import re

import settings

PATH_MATCH_RE =  re.compile(settings.TOMO_PATH_MATCH_RE)

def absolute_file_paths(directory):
   for dirpath, _, filenames in os.walk(directory):
       for f in filenames:
           yield os.path.abspath(os.path.join(dirpath, f))

def rel_file_paths(directory):
   for dirpath, _, filenames in os.walk(directory):
       for f in filenames:
           yield f

class TomoDVHImporter(BaseImporter):

    NAME = "Tomo DVH"
    HANDLE = "tomodvh"

    def __init__(self, *args, **kwargs):

        self.import_dir = kwargs.pop("dvh_directory", settings.TOMO_DVH_DIRECTORY)

        super(TomoDVHImporter, self).__init__(*args, **kwargs)


    def get_patients(self):

        paths = rel_file_paths(self.import_dir)

        patients = {}
        for path in paths:
            try:
                info = PATH_MATCH_RE.match(path).groupdict()
                patient_id = info["patient_id"]
                patient_id = self.clean_patient_id(patient_id)
                plan_id=info["plan_id"]
                plan = {
                    "name":plan_id,
                }

                if patient_id in patients:
                    patients[patient_id][plan_id] = plan
                else:
                    patients[patient_id] = {
                        plan_id: plan,
                    }
            except:
                pass

        return patients

    def get_patient_plan(self, patient_id, plan_id):
        path = self.find_dvh_path(patient_id, plan_id)
        if path:
            return self.get_plan_data(path)

    def clean_patient_id(self, patient_id):
        return patient_id

    def find_dvh_path(self, patient_id, plan_id):
        """return path of the DVH file corresponding to the input patient_id & plan_id
        This just looks through all the filesin the TOMO_DVH_DIRECTORY and tries
        to match the file name looking for the correct patient/plan.
        """

        paths = absolute_file_paths(settings.TOMO_DVH_DIRECTORY)

        for path in paths:
            if os.path.basename(path).startswith("%s_%s" %(patient_id, plan_id)):
                return path

        raise ValueError("Unable to find valid DVH file for plan {0} for patient {1}".format(patient_id, plan_id))

    def get_plan_data(self, path):
        """return dvh data for input file path."""

        with open(path, "r") as f:

            info = PATH_MATCH_RE.match(os.path.basename(path)).groupdict()
            header_data = f.readline()

            if header_data.find("Absolute Volume (cc)") < 0:
                raise ValueError("Invalid volume mode. Must be absolute volume in cc's")

            if header_data.find("Dose (Gy)") >= 0:
                multiplier = 100.
            elif header_data.find("Dose (cGy)") >= 0:
                multiplier = 1.
            else:
                raise ValueError("Unknown dose unit")

            f.seek(0)

            data_frame = pandas.read_csv(f, sep=",")

            plan = {
                "name": info["plan_id"],
                "id": info["plan_id"],
                "structures": self.structures_from_data_frame(data_frame, multiplier)
            }


        return plan


    def structures_from_data_frame(self, data_frame, multiplier):
        names = [x.replace("(STANDARD)","") for x in data_frame.columns[::3].values]
        structures = []
        for i,name in enumerate(names):
            doses = multiplier*data_frame.ix[:,i*3+1].values
            volumes = data_frame.ix[:,i*3+2].values
            maxv = volumes.max()

            #tomo dvh's stop when relative volume hits 1 so push on an extra point
            #to ensure a zero dose point
            structures.append( {
                "name":name,
                "volume":volumes.max(),
                "dvh":{
                    "doses":[0]+list(doses),
                    "volumes":[1]+list(volumes/maxv),
                },
            })
        return structures

    def structures_from_data_frame_(self, data_frame, bin_width):

        grouped = data_frame.groupby("struct")
        dvh_volumes = grouped.volume
        dvh_doses = grouped.dose

        structures = []
        for name in grouped.groups.keys():

            volumes = numpy.array(dvh_volumes.get_group(name), dtype=numpy.float)

            if not dvh.monotonic_decreasing(volumes):
                raise ValueError("DVH must be exported as a cumulative dvh with absolute volume")

            struct_volume = volumes.max()

            doses =  numpy.array(dvh_doses.get_group(name))
            struct_dvh =  dvh.DVH(doses, volumes)

            structures.append({
                "name":name,
                "volume":struct_volume,
                "dvh":{
                    "doses":struct_dvh.doses.tolist(),
                    "volumes":struct_dvh.cum_volumes.tolist(),
                },
            })

        return structures
