#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_tomodvh
----------------------------------

Tests for `tomodvh` module.
"""

import unittest

import tomodvh.tomodvh as tomodvh


class TestTomoDVH(unittest.TestCase):

    def setUp(self):
        self.maxDiff = None
        self.all_patients = {
            "12345678": {
                "testplan": {
                    "name":"testplan",
                },
                "testplancGy": {
                    "name":"testplancGy",
                },
            },

            "87654321": {
                "plantest": {
                    "name":"plantest",
                },
            },

            "09090909": {
                "badunit": {
                    "name":"badunit",
                },

                "badvolunit": {
                    "name":"badvolunit",
                },
            },
        }


        self.patient_id = "12345678"
        self.patient_plan_id = "testplan"
        self.structures = {
            "external":{"volume": 13399.647},
            "CTV60":{"volume": 236.12185},
            "LPAROTID":{"volume": 22.660336},
            "RPAROTID":{"volume": 24.65616},
            "LSUB":{"volume": None},
            "CORD":{"volume": None},
            "BRSTEM":{"volume": None},
            "REYE":{"volume": None},
            "LEYE":{"volume": None},
            "RLENS":{"volume": None},
            "LLENS":{"volume": None},
            "ROPTIC":{"volume": None},
            "LOPTIC":{"volume": None},
            "CHIASM":{"volume": None},
            "LARYNX":{"volume": None},
            "MANDIBLE":{"volume": None},
            "ESOPHAGUS":{"volume": None},
            "POSTCRICOID":{"volume": None},
            "PTV60":{"volume": None},
            "Density1":{"volume": None},
            "Density2":{"volume": None},
            "Density0":{"volume": None},
            "PRVBRSTEM":{"volume": None},
            "PRVCORD":{"volume": None},
            "BRAINspare":{"volume": None},
            "LIPS":{"volume": None},
            "Post Spare":{"volume": None},
            "Hair Spare":{"volume": None},
            "optPTV60":{"volume": None},
            "RING":{"volume": None},
            "Ant Spare":{"volume": None},
            "Skin":{"volume": None},
        }

        self.importer = tomodvh.TomoDVHImporter()

    def test_patient_ids(self):
        patients =  self.importer.get_patients().keys()
        self.assertSetEqual(set(patients), set(self.all_patients.keys()))

    def test_patient_plans(self):
        patients =  self.importer.get_patients()
        self.assertDictEqual(patients, self.all_patients)

    def test_structs(self):
        plan = self.importer.get_patient_plan(self.patient_id, self.patient_plan_id)
        self.assertSetEqual(set([x['name'] for x in plan["structures"]]),set(self.structures.keys()))

    def test_volumes(self):
        plan = self.importer.get_patient_plan(self.patient_id, self.patient_plan_id)
        for name, props in self.structures.iteritems():
            if props["volume"] is not None:
                for s in plan['structures']:
                    if s['name'] == name:
                        break
                else:
                    raise ValueError("missing structure")
                self.assertAlmostEqual(s["volume"], props["volume"], places=3)

    def test_Gy_input(self):
        plan = self.importer.get_patient_plan(self.patient_id, "testplan")

        for s in plan['structures']:
            if s['name'] == "CTV60":
                break
        else:
            raise ValueError("missing structure")
        self.assertAlmostEqual(s['dvh']["doses"][1], 59.31365*100, places=3)

    def test_cGy_input(self):
        plan = self.importer.get_patient_plan(self.patient_id, "testplancGy")

        for s in plan['structures']:
            if s['name'] == "CTV60":
                break
        else:
            raise ValueError("missing structure")
        self.assertAlmostEqual(s['dvh']["doses"][1], 59.31365, places=3)

    def test_bad_vol_unit(self):
        with self.assertRaises(ValueError):
            plan = self.importer.get_patient_plan("09090909", "badvolunit")

    def test_number_bins_correct(self):
        plan = self.importer.get_patient_plan(self.patient_id, "testplan")
        for s in plan['structures']:
            if s['name'] == "CTV60":
                break
        else:
            raise ValueError("missing structure")

        dvh_data = s["dvh"]
        self.assertEqual(len(dvh_data["doses"]), len(dvh_data["volumes"]))

    def test_missing_file(self):
        with self.assertRaises(ValueError):
            self.importer.find_dvh_path("foo", "bar")

if __name__ == '__main__':
    unittest.main()
